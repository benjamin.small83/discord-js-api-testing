const Discord = require('discord.js');
const client = new Discord.Client();

/*
const fs = require("fs");

fs.readdir("/app/", (err, files) => {
    files.forEach(file => {
        console.log(file);
    });
});
 */

const config = require("/app/config.json");

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.guilds.forEach(guild => {
        guild.channels.forEach(channel => {

            if(channel.name != "General")
                return;

            channel.join()
                .then(conn => {
                    conn.playFile("/app/resources/ba-dun-tss.mp3", (err, intent) => {
                        console.log(err);
                    });
                })
                .catch(err => {
                    console.log(err);
                });


        });
    });
});

client.on('message', msg => {
    if (msg.content === 'ping') {
        msg.reply('pong');
    }
});

client.login(config['api_key']);